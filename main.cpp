#include <iostream>
#include <fstream>
#include <map>
#include <cassert>



void analizeLine(std::map<char, int>& m, std::string s)
{
    for(auto a : s)
        if((a>=65&&a<=90)||(a>=97&&a<=122))
        {
            if ( m.find(a) == m.end() )
                m[a] = 1;
            else
                m.at(a) += 1;
        }

}
int main(int argc, const char * argv[]) {
    std::string line;
    std::map<char, int> m;

    std::ifstream in("./Hamlet.txt");
    if (in.is_open())
    {
        while (getline(in, line))
        {
            analizeLine(m,line);
        }
    }
    in.close();
    for(auto& a : m)
        std::cout << a.first << ":" << a.second << std::endl;


    return 0;
}

